# About

This is an attempt at a formalization of some concepts of higher
category theory.

# Source library

This formalization uses the
[agda-base library](https://github.com/pcapriotti/agda-base), specifically
[this commit](https://github.com/pcapriotti/agda-base/commit/bbbc3bfb2f80ad08c8e608cccfa14b83ea3d258c).

# Installation of agda-base

If you want to compile this repository, you will need to download the
agda-base library and tell your Agda installation where to find it on
your computer. The easiest way to do this is probably to use
[agda-pkg](https://github.com/agda/agda-pkg).

You can run the following commands in a terminal to install agda-pkg
and the agda-base library:

```
$ pip install agda-pkg
$ apkg init
$ apkg --github https://github.com/pcapriotti/agda-base
```

After you have installed the agda-base library, you can clone this
repository as usual and it should compile.
