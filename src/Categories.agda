{-# OPTIONS --without-K #-}

{- This file contains the definition
of a category -}

module Categories where

open import hott.level using (HSet)
open import level using (Level; lsuc; _⊔_)
open import sum using (Σ; _,_; proj₁; proj₂)
open import equality using (_≡_)

private
  variable
    i j : Level

record Category : Set (lsuc (i ⊔ j)) where
  field
    Ob   : Set i
    Hom  : Ob → Ob → HSet j
    Id   : (x : Ob) → proj₁ (Hom x x)
    Comp : {x y z : Ob}
         → proj₁ (Hom y z)
         → proj₁ (Hom x y)
         → proj₁ (Hom x z)
    IdIsRightNeutral  : {x y : Ob} {f : proj₁ (Hom x y)}
                      → Comp f (Id x) ≡ f
    IdIsLeftNeutral   : {x y : Ob} {f : proj₁ (Hom x y)}
                      → Comp (Id y) f ≡ f
    CompIsAssociative : {x y z w : Ob} {f : proj₁ (Hom x y)}
                      → {g : proj₁ (Hom y z)} {h : proj₁ (Hom z w)}
                      → Comp h (Comp g f) ≡ Comp (Comp h g) f

open Category {{...}} public

Ob' : Category {i} {j} → Set i
Ob' C = Ob ⦃ C ⦄

_⟶_ : ⦃ C : Category {i} {j} ⦄ → Ob' C → Ob' C → Set j
x ⟶ y = proj₁ (Hom x y)

infixr 5 _∘_
_∘_ :  ⦃ C : Category {i} {j} ⦄ {x y z : Ob' C}
    →  proj₁ (Hom y z) → proj₁ (Hom x y) → proj₁ (Hom x z)
_∘_ = Comp
