{- Imports everything in the repository.
Useful to make sure that everything typechecks. -}

{-# OPTIONS --without-K #-}

module Everything where

import Categories
import Categories.CAT
import Categories.Functor
import Categories.Linear
import Categories.Opposite
import Categories.SET
import Categories.Simplex
import Categories.Simplex.Identities
import Functors
import Functors.Composition
import Functors.Hom
import Functors.Identity
import Functors.Opposite
import NatTrans
import SimplicialSets.Boundaries
import SimplicialSets.Core
import SimplicialSets.Horns
import SimplicialSets.Identities
import SimplicialSets.Nerve
import SimplicialSets.KanComplexes
import Util.Fin
import Util.Hott
import Util.Nat
