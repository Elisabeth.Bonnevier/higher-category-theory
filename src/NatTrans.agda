{-# OPTIONS --without-K #-}

{- This file contains the definition
of a natural transformations between functors -}

module NatTrans where

open import Categories
open import Functors

open import equality using (_≡_)
open import hott using (h; h↑; Π-level; Π-level-impl; Σ-level)
open import level using (Level; _⊔_)
open import sum using (Σ; proj₁; proj₂)

private
  variable
    i j k l : Level

Trans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
      → (F G : Functor C D) → Set (i ⊔ l)
Trans F G = (x : Ob) → proj₁ (Hom (F # x) (G # x))

TransIsh2 : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
          → (F G : Functor C D)
          → h 2 (Trans F G)
TransIsh2 F G = Π-level (λ x → proj₂ (Hom (F # x) (G # x)))

IsNatTrans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
           → (F G : Functor C D) → Trans F G → Set (i ⊔ j ⊔ l)
IsNatTrans F G η = {x y : Ob} {f : proj₁ (Hom x y)} → G $ f ∘ η x ≡ η y ∘ F $ f

IsNatTransIsh1 : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
               → (F G : Functor C D) → (μ : Trans F G)
               → h 1 (IsNatTrans F G μ)
IsNatTransIsh1 F G μ = Π-level-impl (λ x →
                         Π-level-impl (λ y →
                           Π-level-impl (λ f →
                             proj₂ (Hom (F # x) (G # y)) _ _)))

NatTrans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
         → (F G : Functor C D) → Set (i ⊔ j ⊔ l)
NatTrans F G = Σ (Trans F G) (IsNatTrans F G)

NatTransIsSet : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
              → (F G : Functor C D) → h 2 (NatTrans F G)
NatTransIsSet F G = Σ-level (TransIsh2 F G) λ μ → h↑ (IsNatTransIsh1 F G _)

