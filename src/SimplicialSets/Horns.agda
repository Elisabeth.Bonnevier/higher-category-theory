{-# OPTIONS --without-K #-}

{- This file contains the definition
of boundaries of the standard simplices -}

module SimplicialSets.Horns where

open import Categories hiding (_∘_)
open import Categories.Opposite
open import Categories.Simplex
open import Functors
open import SimplicialSets.Core
open import SimplicialSets.Boundaries
open import Util.Hott

open import equality using (_≡_; refl)
open import function using (_∘_; funext)
open import hott.level
  using (Π-level; Π-level-impl; Σ-level; h; h↑; fin-set; ⊥-prop)
open import sets.empty
open import sets.fin using (Fin; _≟_)
open import sets.nat using (ℕ; suc)
open import sum using (Σ; _,_; proj₁; proj₂)

private
  variable
    m n k : ℕ

not-in-horn : (i : Fin (suc n))
            → (f : Fin (suc m) → Fin (suc n))
            → Set
not-in-horn {n} {m} i f = (k : Fin (suc n)) → ¬ k ≡ i → Σ (Fin (suc m)) (λ j → f j ≡ k)

in-horn : (i : Fin (suc n))
        → (f : Fin (suc m) → Fin (suc n))
        → Set
in-horn i f = ¬ (not-in-horn i f)

in-horn-is-h1 : {i : Fin (suc n)}
              → {f : Fin (suc m) → Fin (suc n)}
              → h 1 (in-horn i f)
in-horn-is-h1 = Π-level (λ _ → ⊥-prop)

comp-not⇒not : {i : Fin (suc k)}
             → {f : Fin (suc m) → Fin (suc n)}
             → {g : Fin (suc n) → Fin (suc k)}
             → not-in-horn i (g ∘ f)
             → not-in-horn i g
comp-not⇒not {i = i} {f = f} g∘f-not j j≠i
  = f (proj₁ (g∘f-not j j≠i)) , proj₂ (g∘f-not j j≠i)

comp-in-horn : {i : Fin (suc k)}
             → {f : Fin (suc m) → Fin (suc n)}
             → {g : Fin (suc n) → Fin (suc k)}
             → in-horn i g
             → in-horn i (g ∘ f)
comp-in-horn g-in-horn g∘f-not-in-horn
  = g-in-horn (comp-not⇒not g∘f-not-in-horn)

horn : (n : ℕ) → Fin (suc n) → ℕ → Set
horn n i m = Σ (m ↗ n) (λ f → in-horn i (proj₁ f))

horn-is-h2 : {n : ℕ} {i : Fin (suc n)} {m : ℕ}
           → h 2 (horn n i m)
horn-is-h2 = Σ-level ↗-is-h2 λ _ → h↑ (Π-level (λ _ → ⊥-prop))

-- Horns

Λ⁽_⁾₍_₎ : (n : ℕ) → Fin (suc n) → SimplicialSet
ObjectMap Λ⁽ n ⁾₍ i ₎ m = horn n i m , horn-is-h2
MorphismMap Λ⁽ n ⁾₍ i ₎ f (g , g-in-horn) = g ∘↗ f , comp-in-horn g-in-horn
RespectsIdentities Λ⁽ n ⁾₍ i ₎ = funext (λ _ → Σ-h1-≡ (λ _ → in-horn-is-h1) refl)
RespectsComposition Λ⁽ n ⁾₍ i ₎ {f = f}
  = funext (λ h → Σ-h1-≡ (λ _ → in-horn-is-h1)
                         (CompIsAssociative ⦃ opp Δ ⦄ {f = proj₁ h} {g = f}))

-- Inclusion of horn into the boundary

surj⇒not-in-horn : (i : Fin (suc n))
                 → (f : Fin (suc m) → Fin (suc n))
                 → is-surjective f
                 → not-in-horn i f
surj⇒not-in-horn i f f-surj k k≠i = f-surj k

in-horn⇒not-surj : (i : Fin (suc n))
                 → (f : Fin (suc m) → Fin (suc n))
                 → in-horn i f
                 → is-not-surjective f
in-horn⇒not-surj i f f-in-horn
  = λ f-surj → f-in-horn (surj⇒not-in-horn i f f-surj)

Λ↪∂Δ : (n : ℕ) (i : Fin (suc n)) → Λ⁽ n ⁾₍ i ₎ ⟶ ∂Δ⁽ n ⁾
Λ↪∂Δ n i = (λ m f → proj₁ f , in-horn⇒not-surj i (proj₁ (proj₁ f)) (proj₂ f)) ,
           refl

