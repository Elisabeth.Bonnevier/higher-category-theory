{-# OPTIONS --without-K #-}

{- This file contains the definition
of simplicial sets -}

module SimplicialSets.Core where

open import Categories
open import Categories.Functor
open import Categories.Opposite
open import Categories.SET
open import Categories.Simplex
open import Functors
open import Functors.Hom

open import level using (lzero; lsuc)
open import sets.fin using (Fin; zero; suc)
open import sets.nat using (ℕ; zero; suc)
open import sum using (proj₁)

private
  variable
    n : ℕ

instance
  Δ^op : Category
  Δ^op = opp Δ


SimplicialSet : Set (lsuc lzero)
SimplicialSet = Functor Δ^op 𝕊𝔼𝕋

-- The standard n-simplex

Δ⁽_⁾ : ℕ → SimplicialSet
Δ⁽ n ⁾ = Hom[-, n ]

-- Convenient notation

_₍_₎ : SimplicialSet → ℕ → Set
S ₍ n ₎ = proj₁ (S # n)

-- The face maps

d : ⦃ S : SimplicialSet ⦄
  → (i : [ suc n ])
  → S ₍ suc n ₎ → S ₍ n ₎
d ⦃ S ⦄ i = S $ (δ i)

-- The degeneracy maps

s : ⦃ S : SimplicialSet ⦄
  → (i : [ n ])
  → S ₍ n ₎ → S ₍ suc n ₎
s ⦃ S ⦄ i = S $ (σ i)

-- The category of simplicial sets

instance
  𝕊𝔼𝕋Δ : Category
  𝕊𝔼𝕋Δ = [ Δ^op , 𝕊𝔼𝕋 ]
