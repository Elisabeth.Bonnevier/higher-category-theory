{-# OPTIONS --without-K #-}

{- This file contains the construction of
the nerve of a category as a simplicial set -}

module SimplicialSets.Nerve where

open import Categories
open import Categories.CAT
open import Categories.Linear
open import Categories.Opposite
open import Categories.SET
open import Categories.Simplex
open import Functors
open import Functors.Composition
open import Functors.Hom
open import Functors.Opposite
open import SimplicialSets.Core
open import Util.Fin

open import equality using (_≡_; refl; sym)
open import function using (funext)
open import hott using (h)
open import level using (Level; lsuc; lzero; _⊔_; ↑)
open import sets.fin using (Fin; zero; suc)
open import sets.nat using (ℕ; zero; suc; z≤n)
open import sum using (Σ; _,_; _×_; proj₁; proj₂)

private
  variable
    i j : Level

-- The nerve of a category

Nerve : (C : Category) → h 2 (Ob' C) → SimplicialSet
Nerve C h2-ObC = _□_ ⦃ opp Δ ⦄ ⦃ opp ℂ𝔸𝕋 ⦄ ⦃ 𝕊𝔼𝕋 ⦄ Hom[-, (C , h2-ObC) ] (fun-opp 𝕃[])


-- How vertices and edges in the nerve correspond to objects and morphisms in C

-- Every object in C determines a functor from 𝕃 0 to C
ob-functor : (C : Category {i} {j})
           → (x : Ob' C)
           → Functor (𝕃 0) C
ObjectMap (ob-functor C x) zero = x
MorphismMap (ob-functor C x) {zero} {zero} 0≤0 = Id ⦃ C ⦄ x
RespectsIdentities (ob-functor C x) {zero} = refl
RespectsComposition (ob-functor C x) {zero} {zero} {zero}
  = sym (IdIsRightNeutral ⦃ C ⦄)

-- Every morphism in C determines a functor from 𝕃 1 to C
mor-functor : (C : Category {i} {j})
            → (x y : Ob' C)
            → (f : proj₁ (Hom ⦃ C ⦄ x y))
            → Functor (𝕃 1) C
ObjectMap (mor-functor C x y f) zero = x
ObjectMap (mor-functor C x y f) (suc zero) = y
MorphismMap (mor-functor C x y f) {zero} {zero} 0≤0 = Id ⦃ C ⦄ x 
MorphismMap (mor-functor C x y f) {zero} {suc zero} 0≤1 = f
MorphismMap (mor-functor C x y f) {suc zero} {suc zero} 1≤1 = Id ⦃ C ⦄ y
RespectsIdentities (mor-functor C x y f) {zero} = refl
RespectsIdentities (mor-functor C x y f) {suc zero} = refl
RespectsComposition (mor-functor C x y f) {zero} {zero} {zero}
  = sym (IdIsRightNeutral ⦃ C ⦄)
RespectsComposition (mor-functor C x y f) {zero} {zero} {suc zero}
  = sym (IdIsRightNeutral ⦃ C ⦄)
RespectsComposition (mor-functor C x y f) {zero} {suc zero} {suc zero}
  = sym (IdIsLeftNeutral ⦃ C ⦄)
RespectsComposition (mor-functor C x y f) {suc zero} {suc zero} {suc zero}
  = sym (IdIsLeftNeutral ⦃ C ⦄)

-- The face d₀ of a morphism f : x → y is the target y
target-is-face : (C : Category) → (p : h 2 (Ob' C))
               → (x y : Ob' C)
               → (f : proj₁ (Hom ⦃ C ⦄ x y))
               → ObjectMap (d ⦃ Nerve C p ⦄ zero (mor-functor C x y f)) zero ≡ y
target-is-face C p x y f = refl

-- The face d₁ of a morphism f : x → y is the source x
source-is-face : (C : Category) → (p : h 2 (Ob' C))
               → (x y : Ob' C)
               → (f : proj₁ (Hom ⦃ C ⦄ x y))
               → ObjectMap (d ⦃ Nerve C p ⦄ (suc zero) (mor-functor C x y f)) zero ≡ x
source-is-face C p x y f = refl

-- The degenerate simplex s₀ of an object x ∈ C is the identity morphism
deg-is-id : (C : Category) → (p : h 2 (Ob' C))
          → (x : Ob' C)
          → MorphismMap (s ⦃ Nerve C p ⦄ zero (ob-functor C x)) (z≤n {suc zero})
          ≡ Id ⦃ C ⦄ x
deg-is-id C p x = refl

