{-# OPTIONS --without-K #-}

{- This file contains the definition
of Kan complexes -}

module SimplicialSets.KanComplexes where

{-
A Kan complex is a simplicial set S that satisies the following condition:
for n > 0 and 0 ≤ i ≤ n, any map of simplicial sets σ₀ : Λ^n_i → S can be
extended to a map σ : Δ^n → S
-}

open import Categories
open import Categories.Functor
open import Categories.SET
open import SimplicialSets.Boundaries
open import SimplicialSets.Core
open import SimplicialSets.Horns

open import equality using (_≡_)
open import level using (lsuc; lzero)
open import sets.empty
open import sets.fin using (Fin; zero; suc; last-fin)
open import sets.nat using (ℕ; zero; suc)
open import sum using (Σ)


KanComplex : Set (lsuc lzero)
KanComplex = Σ SimplicialSet
               (λ S → (n : ℕ) (i : Fin (suc (suc n)))
                    → (σ₀ : Λ⁽ suc n ⁾₍ i ₎ ⟶ S)
                    → Σ (Δ⁽ suc n ⁾ ⟶ S)
                        λ σ → σ₀ ≡ _∙_ ⦃ F = Λ⁽ suc n ⁾₍ i ₎ ⦄
                                       ⦃ G = Δ⁽ suc n ⁾ ⦄
                                       ⦃ H = S ⦄
                                       σ
                                  (_∙_ ⦃ F = Λ⁽ suc n ⁾₍ i ₎ ⦄
                                       ⦃ G = ∂Δ⁽ suc n ⁾ ⦄
                                       ⦃ H = Δ⁽ suc n ⁾ ⦄
                                       (∂Δ↪Δ (suc n))
                                       (Λ↪∂Δ (suc n) i)))

{-
A quasicategory, or a weak Kan complex is a simplicial set S that satisfies
the following condition:
for n > 0 and 0 < i < n, any map of simplicial sets σ₀ : Λ^n_i → S can be
extended to a map σ : Δ^n → S
-}

quasicategory : Set (lsuc lzero)
quasicategory = Σ SimplicialSet
                  (λ S → (n : ℕ) (i : Fin (suc (suc n)))
                       → ¬ i ≡ zero → ¬ i ≡ last-fin
                       → (σ₀ : Λ⁽ suc n ⁾₍ i ₎ ⟶ S)
                       → Σ (Δ⁽ suc n ⁾ ⟶ S)
                           λ σ → σ₀ ≡ _∙_ ⦃ F = Λ⁽ suc n ⁾₍ i ₎ ⦄
                                          ⦃ G = Δ⁽ suc n ⁾ ⦄
                                          ⦃ H = S ⦄
                                          σ
                                     (_∙_ ⦃ F = Λ⁽ suc n ⁾₍ i ₎ ⦄
                                          ⦃ G = ∂Δ⁽ suc n ⁾ ⦄
                                          ⦃ H = Δ⁽ suc n ⁾ ⦄
                                          (∂Δ↪Δ (suc n))
                                          (Λ↪∂Δ (suc n) i)))
