{-# OPTIONS --without-K #-}

{- This file contains the simplicial
identities -}

module SimplicialSets.Identities where

open import Categories
open import Categories.Opposite
open import Categories.SET
open import Categories.Simplex
open import Categories.Simplex.Identities
open import Functors
open import SimplicialSets.Core
open import Util.Fin

import equality
open equality using (_≡_; refl; sym; ap)
open equality.≡-Reasoning
open import function hiding (_$_) renaming (_∘_ to _∘f_)
open import sets.empty
open import sets.fin using (Fin; zero; suc; _<_; last-fin; pred; raise)
open import sets.nat using (ℕ; zero; suc)

private
  variable
    n : ℕ

-- For n ≥ 2 and 0 ≤ i < j ≤ n we have d i ∘ d j = d (j - 1) ∘ d i

di∘dj=dj-1∘di : ⦃ S : SimplicialSet ⦄
              → (i j : [ suc (suc n) ]) → i < j
              → (p : ¬ i ≡ last-fin) → (q : ¬ j ≡ zero)
              → d (lower i p) ∘f d j ≡ d (pred j q) ∘f d i
di∘dj=dj-1∘di ⦃ S ⦄ i j i<j i≠last j≠0 =
  begin
    d (lower i i≠last) ∘f d j
  ≡⟨ refl ⟩
    (S $ δ (lower i i≠last)) ∘f (S $ δ j)
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (δ j ∘ δ (lower i i≠last))
  ≡⟨ ap (MorphismMap S) (δjδi=δiδj-1 i j i<j i≠last j≠0) ⟩
    S $ (δ i ∘ δ (pred j j≠0))
  ≡⟨ RespectsComposition S ⟩
    (S $ δ (pred j j≠0)) ∘f (S $ δ i)
  ≡⟨ refl ⟩
    d (pred j j≠0) ∘f d i
  ∎


-- For 0 ≤ i ≤ j ≤ n we have s i ∘ s j = s (j + 1) ∘ s i

si∘sj=sj+1∘si : ⦃ S : SimplicialSet ⦄
              → (i j : [ n ]) → i ≤ j
              → s (raise i) ∘f s j ≡ s (suc j) ∘f s i
si∘sj=sj+1∘si ⦃ S ⦄ i j i≤j =
  begin
    s (raise i) ∘f s j
  ≡⟨ refl ⟩
    (S $ σ (raise i)) ∘f (S $ σ j)
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (σ (raise i) ∘ σ j)
  ≡⟨ ap (MorphismMap S) (σjσi=σiσj+1 i j i≤j) ⟩
    S $ (σ (suc j) ∘ σ i)
  ≡⟨ RespectsComposition S ⟩
    (S $ σ (suc j)) ∘f (S $ σ i)
  ≡⟨ refl ⟩
    s (suc j) ∘f s i
  ∎

-- For 0 ≤ i < j ≤ n we have d i ∘ s j = s (j - 1) ∘ d i

di∘sj=sj-1∘di : ⦃ S : SimplicialSet ⦄
              → (i j : [ suc n ]) → i < j
              → (p : ¬ j ≡ zero)
              → d (raise i) ∘f s j ≡ s (pred j p) ∘f d i
di∘sj=sj-1∘di ⦃ S ⦄ i j i<j j≠0 =
  begin
    d (raise i) ∘f s j
  ≡⟨ refl ⟩
    (S $ δ (raise i)) ∘f (S $ σ j)
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (σ j ∘ δ (raise i))
  ≡⟨ ap (MorphismMap S) (σj∘δi=δi∘σj-1 i j i<j j≠0) ⟩
    S $ (δ i ∘ σ (pred j j≠0))
  ≡⟨ RespectsComposition S ⟩
    (S $ σ (pred j j≠0)) ∘f (S $ δ i)
  ≡⟨ refl ⟩
    s (pred j j≠0) ∘f d i
  ∎

-- For 0 ≤ i ≤ n we have d i ∘ s i = id

di∘si=id : ⦃ S : SimplicialSet ⦄
         → (i : [ n ])
         → d (raise i) ∘f s i ≡ id
di∘si=id {n} ⦃ S ⦄ i =
  begin
    d (raise i) ∘f s i
  ≡⟨ refl ⟩
    (S $ δ (raise i)) ∘f (S $ σ i)
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (σ i ∘ δ (raise i))
  ≡⟨ ap (MorphismMap S) (σi∘δi=id i) ⟩
    S $ Id ⦃ Δ ⦄ n
  ≡⟨ RespectsIdentities S ⟩
    id
  ∎

-- For 0 ≤ i ≤ n we have d (i + 1) ∘ s i = id

di+1∘si=id : ⦃ S : SimplicialSet ⦄
           → (i : [ n ])
           → d (suc i) ∘f s i ≡ id
di+1∘si=id {n} ⦃ S ⦄ i =
  begin
    d (suc i) ∘f s i
  ≡⟨ refl ⟩
    (S $ δ (suc i)) ∘f (S $ σ i)
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (σ i ∘ δ (suc i))
  ≡⟨ ap (MorphismMap S) (σi∘δi+1=id i) ⟩
    S $ Id ⦃ Δ ⦄ n
  ≡⟨ RespectsIdentities S ⟩
    id
  ∎

-- For 0 ≤ i, j ≤ n with j + 1 < i we have d i ∘ s j = s j ∘ d (i - 1)

di∘sj=sj∘di-1 : ⦃ S : SimplicialSet ⦄
              → (i : [ suc (suc n) ])
              → (j : [ n ])
              → suc (suc j) ≤ i
              → (p : ¬ i ≡ zero)
              → d i ∘f s (raise j) ≡ s j ∘f d (pred i p)
di∘sj=sj∘di-1 ⦃ S ⦄ i j j+2≤i i≠0 =
  begin
    d i ∘f s (raise j)
  ≡⟨ refl ⟩
    (S $ δ i) ∘f (S $ σ (raise j))
  ≡⟨ sym (RespectsComposition S) ⟩
    S $ (σ (raise j) ∘ δ i)
  ≡⟨ ap (MorphismMap S) (σj∘δi=δi-1∘σj i j j+2≤i i≠0) ⟩
    S $ (δ (pred i i≠0) ∘ σ j)
  ≡⟨ RespectsComposition S ⟩
    (S $ σ j) ∘f (S $ δ (pred i i≠0))
  ≡⟨ refl ⟩
    s j ∘f d (pred i i≠0)
  ∎
