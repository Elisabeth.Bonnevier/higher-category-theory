{-# OPTIONS --without-K #-}

{- This file contains the definition
of boundaries of the standard simplices -}

module SimplicialSets.Boundaries where

open import Categories hiding (_∘_)
open import Categories.Opposite
open import Categories.SET
open import Categories.Simplex
open import Functors
open import NatTrans
open import SimplicialSets.Core
open import Util.Hott

open import equality using (_≡_; refl; _·_)
open import function using (_∘_; funext)
open import hott.level
  using (Π-level; Π-level-impl; Σ-level; h; h↑; fin-set; ⊥-prop)
open import sets.empty
open import sets.fin using (Fin)
open import sets.nat using (ℕ; suc)
open import sum using (Σ; _,_; proj₁; proj₂)

private
  variable
    m n k : ℕ

is-surjective : (Fin m → Fin n) → Set
is-surjective {m} {n} f = (k : Fin n) → Σ (Fin m) (λ l → f l ≡ k)

is-not-surjective : (Fin m → Fin n) → Set
is-not-surjective f = ¬ (is-surjective f)

is-not-surjective-is-h1 : {f : Fin m → Fin n}
                        → h 1 (is-not-surjective f)
is-not-surjective-is-h1 {f = f} = Π-level λ _ → ⊥-prop

infix 5 _⇻_
_⇻_ : ℕ → ℕ → Set
m ⇻ n = (Σ (m ↗ n) (is-not-surjective ∘ proj₁))

⇻-is-h2 : h 2 (m ⇻ n)
⇻-is-h2 = Σ-level ↗-is-h2 λ (f , _) → h↑ (is-not-surjective-is-h1)

comp-surj⇒surj : {f : Fin m → Fin n}
               → {g : Fin n → Fin k}
               → is-surjective (g ∘ f)
               → is-surjective g
comp-surj⇒surj {f = f} {g = g} g∘f-is-surj l
  = f (proj₁ (g∘f-is-surj l)) , proj₂ (g∘f-is-surj l)

comp-is-not-surjective : {f : Fin m → Fin n}
                       → {g : Fin n → Fin k}
                       → is-not-surjective g
                       → is-not-surjective (g ∘ f)
comp-is-not-surjective g-not-surj g∘f-is-surj = g-not-surj (comp-surj⇒surj g∘f-is-surj)

_∘⇻_ : n ⇻ k → m ⇻ n → m ⇻ k
_∘⇻_ (g , g-not-surj) (f , f-not-surj)
  = g ∘↗ f , comp-is-not-surjective g-not-surj

∂Δ⁽_⁾ : ℕ → SimplicialSet
ObjectMap ∂Δ⁽ n ⁾ m = m ⇻ n , ⇻-is-h2
MorphismMap ∂Δ⁽ n ⁾ f (g , g-not-surj) = g ∘↗ f , comp-is-not-surjective g-not-surj
RespectsIdentities ∂Δ⁽ n ⁾ = funext (λ _ → Σ-h1-≡ (λ _ → is-not-surjective-is-h1) refl)
RespectsComposition ∂Δ⁽ n ⁾ {f = f}
  = funext (λ h → Σ-h1-≡ (λ _ → is-not-surjective-is-h1)
                         (CompIsAssociative ⦃ opp Δ ⦄ {f = proj₁ h} {g = f}))


-- Inclusion of the boundary into the standard simplex

∂Δ↪Δ : (n : ℕ) → ∂Δ⁽ n ⁾ ⟶ Δ⁽ n ⁾
∂Δ↪Δ n = (λ _ f → proj₁ f) , refl
