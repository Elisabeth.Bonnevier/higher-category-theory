{-# OPTIONS --without-K #-}

{- This file contains the definition
of a functor between categories -}

module Functors where

open import Categories
open import Util.Hott

import equality
open equality using (_≡_; refl; subst; subst₂; unapΣ)
open equality.≡-Reasoning
open import function.extensionality using (impl-funext)
open import function.isomorphism using (_≅_; invert≅; sym≅; iso≡)
open import hott using (Σ-level; Π-level; Π-level-impl)
open import level using (Level; lsuc; _⊔_)
open import sum using (Σ; _,_; _×_; proj₁; proj₂)

private
  variable
    i j k l : Level

record Functor (C : Category {i} {j}) (D : Category {k} {l})
  : Set ((i ⊔ j) ⊔ (k ⊔ l)) where
  field
    ObjectMap   : Ob' C → Ob' D
    MorphismMap : {x y : Ob' C}
                → proj₁ (Hom ⦃ C ⦄ x y)
                → proj₁ (Hom ⦃ D ⦄ (ObjectMap x) (ObjectMap y))
    RespectsIdentities  : {x : Ob' C}
                        → MorphismMap (Id ⦃ C ⦄ x) ≡ Id ⦃ D ⦄ (ObjectMap x)
    RespectsComposition : {x y z : Ob' C}
                        → {f : proj₁ (Hom ⦃ C ⦄ x y)}
                        → {g : proj₁ (Hom ⦃ C ⦄ y z)}
                        → MorphismMap (Comp ⦃ C ⦄ g f)
                        ≡ Comp ⦃ D ⦄ (MorphismMap g) (MorphismMap f)

open Functor public

infixr 6 _#_
_#_ : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
    → Functor C D → Ob' C → Ob' D
F # x = ObjectMap F x

infixr 6 _$_
_$_ : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
    → (F : Functor C D) → {x y : Ob' C}
    → proj₁ (Hom x y) → proj₁ (Hom (F # x) (F # y))
F $ f = MorphismMap F f

-- We encode the type of functors from C to D as a Σ-type
-- to more easily work with equalities between functors

module FunctorIso
  {C : Category {i} {j}}
  {D : Category {k} {l}} where

  FunctorΣ : Set ((i ⊔ j) ⊔ (k ⊔ l))
  FunctorΣ = Σ (Σ (Ob' C → Ob' D)
                  (λ ObMap → (x y : Ob' C)
                    → proj₁ (Hom ⦃ C ⦄ x y)
                    → proj₁ (Hom ⦃ D ⦄ (ObMap x) (ObMap y))))
             (λ (ObMap , MorMap)
               → ({x : Ob' C} → MorMap _ _ (Id ⦃ C ⦄ x) ≡ Id ⦃ D ⦄ (ObMap x))
               × ({x y z : Ob' C}
                 → {f : proj₁ (Hom ⦃ C ⦄ x y)}
                 → {g : proj₁ (Hom ⦃ C ⦄ y z)}
                 → MorMap _ _(Comp ⦃ C ⦄ g f) ≡ Comp ⦃ D ⦄ (MorMap _ _ g) (MorMap _ _ f)))
  
  encode : Functor C D
         → FunctorΣ
  encode F = (ObjectMap F ,
             λ x y → MorphismMap F {x} {y}) ,
             RespectsIdentities F ,
             RespectsComposition F

  decode : FunctorΣ
         → Functor C D
  decode ((ObMap , MorMap) , RespId , RespComp)
    = record {
        ObjectMap = ObMap ;
        MorphismMap = λ {x y} → MorMap x y ;
        RespectsIdentities = RespId ;
        RespectsComposition = RespComp
        }

  encode-decode : ∀ F → encode (decode F) ≡ F
  encode-decode F = refl

  decode-encode : ∀ F → decode (encode F) ≡ F
  decode-encode F = refl

  Functor≅ : FunctorΣ ≅ Functor C D
  _≅_.to Functor≅ = decode
  _≅_.from Functor≅ = encode
  _≅_.iso₁ Functor≅ = encode-decode
  _≅_.iso₂ Functor≅ = decode-encode

open FunctorIso public using (Functor≅)


-- In order to prove that two functors are equal it is enough
-- to prove that the object maps and morphism maps are equal


functor≡ : {C : Category {i} {j}} {D : Category {k} {l}}
         → {F G : Functor C D}
         → (p : ObjectMap F ≡ ObjectMap G)
         → subst (λ ObMap → (x y : Ob' C)
                    → proj₁ (Hom ⦃ C ⦄ x y)
                    → proj₁ (Hom ⦃ D ⦄ (ObMap x) (ObMap y)))
                 p (λ x y → MorphismMap F {x} {y})
         ≡ (λ x y → MorphismMap G {x} {y})
         → F ≡ G
functor≡ {C = C} {D = D} {F = F} {G = G} p q
  = invert≅ (iso≡ (sym≅ Functor≅))
            (Σ-h1-≡ (λ _ → Σ-level
                       (Π-level-impl
                         λ _ → proj₂ (Hom ⦃ D ⦄ _ _) _ _)
                       λ _ → Π-level-impl
                         λ _ → Π-level-impl
                           λ _ → Π-level-impl
                             λ _ → Π-level-impl
                               λ _ → Π-level-impl
                                 λ _ → proj₂ (Hom ⦃ D ⦄ _ _) _ _)
                    (unapΣ (p , q)))


