{-# OPTIONS --without-K #-}

{- This file contains the construction of
the opposite functor -}

module Functors.Opposite where

open import Categories
open import Categories.Opposite
open import Functors

open import level using (Level)

private
  variable
    i j k l : Level

fun-opp : {C : Category {i} {j}} {D : Category {k} {l}}
        → Functor C D
        → Functor (opp C) (opp D)
fun-opp {C = C} {D = D} F = record {
  ObjectMap = ObjectMap F ;
  MorphismMap = MorphismMap F ;
  RespectsIdentities = RespectsIdentities F ;
  RespectsComposition = RespectsComposition F
  }
