{-# OPTIONS --without-K #-}

{- This file contains the construction
of the composition of two functors -}

module Functors.Composition where

open import Categories
open import Functors

import equality
open equality using (_≡_; refl; sym; ap)
open equality.≡-Reasoning
open import level using (Level)

private
  variable
    i j k l m n : Level

_□_ : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄ ⦃ E : Category {m} {n} ⦄
    → Functor D E → Functor C D → Functor C E
ObjectMap (G □ F) x = G # F # x
MorphismMap (G □ F) f = G $ F $ f
RespectsIdentities (G □ F) {x} =
  begin
    G $ F $ Id x
  ≡⟨ ap (G $_) (RespectsIdentities F) ⟩
    G $ Id (F # x)
  ≡⟨ RespectsIdentities G ⟩
    Id (G # F # x)
  ∎
RespectsComposition (G □ F) {f = f} {g = g} =
  begin
    G $ F $ (g ∘ f)
  ≡⟨ ap (G $_) (RespectsComposition F) ⟩
    G $ (F $ g ∘ F $ f)
  ≡⟨ RespectsComposition G ⟩
    (G $ F $ g) ∘ (G $ F $ f)
  ∎

