{-# OPTIONS --without-K #-}

{- This file contains the construction
of the covariant and contravariant hom-functors -}

module Functors.Hom where

open import Categories
open import Categories.Opposite
open import Categories.SET
open import Functors

open import equality using (sym)
open import function.extensionality using (funext)
open import level using (Level; lzero; lsuc; _⊔_)

private
  variable
    i : Level

Hom[_,-] : ⦃ C : Category {i} {lzero} ⦄ → Ob' C → Functor C 𝕊𝔼𝕋
Hom[ a ,-] = record {
  ObjectMap = λ x → Hom a x ;
  MorphismMap = λ f g → f ∘ g ;
  RespectsIdentities = funext (λ g → IdIsLeftNeutral) ;
  RespectsComposition = funext (λ g' → sym CompIsAssociative)
  }

Hom[-,_] : ⦃ C : Category {i} {lzero} ⦄ → Ob' C → Functor (opp C) 𝕊𝔼𝕋
Hom[-, a ] = record {
  ObjectMap = λ x → Hom x a ;
  MorphismMap = λ h g → g ∘ h ;
  RespectsIdentities = funext (λ g → IdIsRightNeutral) ;
  RespectsComposition = funext (λ g' → CompIsAssociative)
  }
