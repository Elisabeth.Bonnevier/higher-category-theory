{-# OPTIONS --without-K #-}

{- This file contains the definition
of the identity functor -}

module Functors.Identity where

open import Categories
open import Functors

open import equality using (refl)
open import level using (Level)

private
  variable
    i j : Level

𝕀𝕕 : (C : Category {i} {j}) → Functor C C
ObjectMap (𝕀𝕕 C) x = x
MorphismMap (𝕀𝕕 C) f = f
RespectsIdentities (𝕀𝕕 C) = refl
RespectsComposition (𝕀𝕕 C) = refl
