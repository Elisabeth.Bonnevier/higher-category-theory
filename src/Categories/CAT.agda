{-# OPTIONS --without-K #-}

{- This file contains the construction of
the category of small categories and functors -}

module Categories.CAT where

open import Categories
open import Functors
open import Functors.Composition
open import Functors.Identity
open import Util.Hott

open import equality using (refl)
open import hott using (h; h↑; iso-level; Σ-level; Π-level; Π-level-impl)
open import level using (Level; lsuc; _⊔_)
open import sum using (Σ; _,_; proj₁; proj₂)

private
  variable
    i j k l : Level

-- The type of functors between two categories for which
-- the type of objects in the target category is an h-set, is an h-set

functor-is-h2 : (C : Category {i} {j}) (D : Category {k} {l})
              → h 2 (Ob' D)
              → h 2 (Functor C D)
functor-is-h2 C D h2-ObD =
  iso-level
    Functor≅
    (Σ-level
      (Σ-level
        (Π-level (λ _ → h2-ObD))
        λ _ → Π-level
          (λ _ → Π-level
            λ _ → Π-level
              (λ _ → proj₂ (Hom ⦃ D ⦄ _ _))))
      λ _ → Σ-level
        (Π-level-impl
          λ _ → h↑ (proj₂ (Hom ⦃ D ⦄ _ _) _ _))
        λ _ → Π-level-impl
          (λ _ → Π-level-impl
            λ _ → Π-level-impl
              λ _ → Π-level-impl
                λ _ → Π-level-impl
                  λ _ → h↑ (proj₂ (Hom ⦃ D ⦄ _ _) _ _)))

-- ℂ𝔸𝕋 is the category of categories for which the type of
-- objects is an h-set. We need this restriction on the
-- type of objects to conclude that the type of functors
-- will be an h-set.

instance
  ℂ𝔸𝕋 : {i j : Level} → Category {lsuc (i ⊔ j)} {i ⊔ j}
  ℂ𝔸𝕋 {i} {j} = record {
    Ob = Σ (Category {i} {j}) λ C → h 2 (Ob' C);
    Hom = λ (C , _) (D , h2-ObD)
          → Functor C D , functor-is-h2 C D h2-ObD ;
    Id = λ (C , h2-ObC) → 𝕀𝕕 C ;
    Comp = λ {(C , _) (D , _) (E , _)} G F → _□_ ⦃ C ⦄ ⦃ D ⦄ ⦃ E ⦄ G F ;
    IdIsRightNeutral = functor≡ refl refl ;
    IdIsLeftNeutral = functor≡ refl refl ;
    CompIsAssociative = functor≡ refl refl
    }
