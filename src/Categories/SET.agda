{-# OPTIONS --without-K #-}

{- This file contains the construction
of the category of h-sets and functions -}

module Categories.SET where

open import Categories hiding (_∘_)

open import equality using (_≡_; refl)
open import function using (_∘_; id)
open import hott.level using (HSet; Π-level)
open import level using (lzero)
open import sum using (Σ; _,_; proj₁; proj₂)


instance
  𝕊𝔼𝕋 : Category
  𝕊𝔼𝕋 = record {
    Ob = HSet lzero ;
    Hom = λ A B → ((proj₁ A → proj₁ B) , Π-level (λ _ → proj₂ B)) ;
    Id = λ _ → id ;
    Comp = _∘_ ;
    IdIsRightNeutral = refl ;
    IdIsLeftNeutral = refl ;
    CompIsAssociative = refl
    }

