{-# OPTIONS --without-K #-}

{- This file contains the construction
of the simplex category -}

module Categories.Simplex where

open import Categories hiding (_∘_)
open import Util.Fin
open import Util.Nat

open import decidable
open import equality using (_≡_; refl; subst; subst₂)
open import function using (_∘_; id)
open import hott.level
  using (Π-level; Π-level-impl; Σ-level; h; h↑; fin-set)
open import sets.empty using (⊥-elim)
open import sets.fin
  using (Fin; zero; suc; toℕ; raise; pred)
open import sets.nat using (ℕ; zero; suc)
open import sets.nat.ordering as N hiding (_≤_; _<_)
open import sum using (Σ; _,_)

private
  variable
    m n k : ℕ

is-mono-incr : (Fin m → Fin n) → Set
is-mono-incr {m = m} f = {i j : Fin m} → i ≤ j → f i ≤ f j

is-mono-incr-is-h1 : (f : Fin m → Fin n)
                    → h 1 (is-mono-incr f)
is-mono-incr-is-h1 f = Π-level-impl
                               λ _ → Π-level-impl
                                       (λ _ → Π-level
                                                (λ _ → ≤-level))
-- [n] denotes the set {0,...,n}
[_] : ℕ → Set
[ n ] = Fin (suc n)

infix 5 _↗_
_↗_ : ℕ → ℕ → Set
m ↗ n = Σ ([ m ] → [ n ]) is-mono-incr

↗-is-h2 : {m n : ℕ} → h 2 (m ↗ n)
↗-is-h2 {m} {n} = Σ-level (Π-level (λ _ → fin-set (suc n)))
                          (λ f → h↑ (is-mono-incr-is-h1 f))


id-is-mono-incr : is-mono-incr {n} {n} id
id-is-mono-incr = λ p → p

comp-is-mono-incr : {f : Fin m → Fin n} {g : Fin n → Fin k}
                   → is-mono-incr f
                   → is-mono-incr g
                   → is-mono-incr (g ∘ f)
comp-is-mono-incr f-is g-is i≤j = g-is (f-is i≤j)

infix 5 _∘↗_
_∘↗_ : n ↗ k → m ↗ n → m ↗ k
_∘↗_ (g , g-is) (f , f-is) = g ∘ f , comp-is-mono-incr f-is g-is

instance
  Δ : Category
  Δ = record {
    Ob = ℕ ;
    Hom = λ m n → (m ↗ n , ↗-is-h2) ;
    Id = λ n → (id , id-is-mono-incr) ;
    Comp = λ g f → g ∘↗ f ;
    IdIsRightNeutral = refl ;
    IdIsLeftNeutral = refl ;
    CompIsAssociative = refl
    }


-- The standard injective maps in Δ

δ-fun : [ n ] → Fin n → [ n ]
δ-fun i j with toℕ i ≤? toℕ j
...          | yes i≤j = suc j
...          | no i≰j  = raise j

δ-is-mono-incr : ∀ i → is-mono-incr (δ-fun {n} i)
δ-is-mono-incr i {j} {j'} j≤j'
  with toℕ i ≤? toℕ j | toℕ i ≤? toℕ j'
...  | yes i≤j | yes i≤j' = s≤s j≤j'
...  | yes i≤j | no  i≰j' = ⊥-elim (i≰j' (trans≤ i≤j j≤j'))
...  | no  i≰j | yes i≤j' = trans≤ (subst (λ x → x N.≤ toℕ j') (toℕ-raise j) j≤j') suc≤
...  | no  i≰j | no  i≰j' = subst₂ N._≤_ (toℕ-raise j) (toℕ-raise j') j≤j'

δ : (i : [ suc n ]) → n ⟶ (suc n)
δ i = (δ-fun i , δ-is-mono-incr i)

-- The standard surjective maps in Δ

σ-fun : [ n ] → [ suc n ] → [ n ]
σ-fun i j with toℕ j ≤? toℕ i
...              | yes j≤i = lower j (≠last j≤i)
...              | no j≰i  = pred j (≰to≠0 j≰i)

σ-is-mono-incr : ∀ i → is-mono-incr (σ-fun {n} i)
σ-is-mono-incr i {j} {j'} j≤j'
  with toℕ j ≤? toℕ i | toℕ j' ≤? toℕ i
...  | yes j≤i | yes j'≤i = subst₂ N._≤_ (toℕ-lower j) (toℕ-lower j') j≤j'
...  | yes j≤i | no  j'≰i = subst₂ N._≤_ (toℕ-lower j)
                                         (toℕ-pred j')
                                         (<pred (trans≤ (s≤s j≤i)
                                                        (≰to> j'≰i)))
...  | no  j≰i | yes j'≤i = ⊥-elim (j≰i (trans≤ j≤j' j'≤i))
...  | no  j≰i | no  j'≰i = subst₂ N._≤_ (toℕ-pred j)
                                         (toℕ-pred j')
                                         (≤pred j≤j')

σ : (i : [ n ]) → (suc n) ⟶ n
σ i = (σ-fun i , σ-is-mono-incr i)
