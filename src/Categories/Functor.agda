{-# OPTIONS --without-K #-}

{- This file contains the definition
of functor categories -}

module Categories.Functor where

open import Categories
open import Functors
open import NatTrans
open import Util.Hott

import equality
open equality using (_≡_; refl; sym; ap)
open equality.≡-Reasoning
open import function.extensionality using (funext)
open import hott.level using (h1⇒prop; Π-level-impl)
open import level using (Level; lsuc; _⊔_)
open import sum using (Σ; _,_; proj₁; proj₂)

private
  variable
    i j k l : Level

id-is-nat-trans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                → (F : Functor C D)
                → {x y : Ob' C} {f : proj₁ (Hom x y)}
                → F $ f ∘ Id (F # x) ≡ Id (F # y) ∘ F $ f
id-is-nat-trans ⦃ D = D ⦄ F {x} {y} {f} =
  begin
    F $ f ∘ Id (F # x)
  ≡⟨ IdIsRightNeutral ⟩
    F $ f
  ≡⟨ sym IdIsLeftNeutral ⟩
    Id (F # y) ∘ F $ f
  ∎

id-trans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
         → (F : Functor C D) → NatTrans F F
id-trans F = (λ x → Id (F # x)) , (id-is-nat-trans F)

{- Vertical composition of natural transformations -}

∙-is-nat-trans : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
               → ⦃ F G H : Functor C D ⦄
               → (μ : NatTrans G H) → (η : NatTrans F G)
               → {x y : Ob' C} {f : proj₁ (Hom x y)}
               → H $ f ∘ ((proj₁ μ x) ∘ (proj₁ η x))
               ≡ ((proj₁ μ y) ∘ (proj₁ η y)) ∘ F $ f
∙-is-nat-trans ⦃ F = F ⦄ ⦃ G = G ⦄ ⦃ H = H ⦄ (μ , μ-is-nat) (η , η-is-nat) {x} {y} {f} =
  begin
    H $ f ∘ ((μ x) ∘ (η x))
  ≡⟨ CompIsAssociative ⟩
    (H $ f ∘ (μ x)) ∘ (η x)
  ≡⟨ ap (_∘ η x) μ-is-nat ⟩
    ((μ y) ∘ G $ f) ∘ (η x)
  ≡⟨ sym CompIsAssociative ⟩
    (μ y) ∘ (G $ f ∘ (η x))
  ≡⟨ ap (μ y ∘_) η-is-nat ⟩
    (μ y) ∘ ((η y) ∘ F $ f)
  ≡⟨ CompIsAssociative ⟩
    ((μ y) ∘ (η y)) ∘ F $ f
  ∎

infixr 5 _∙_
_∙_ : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
    → ⦃ F G H : Functor C D ⦄
    → NatTrans G H → NatTrans F G → NatTrans F H
μ ∙ η = (λ x → proj₁ μ x ∘ proj₁ η x) , ∙-is-nat-trans μ η


{- Identity transformations and vertical composition define a category -}

{- To prove that the identity transformation is left and right
neutral and that composition is associative we need the assumption
that hom-sets in categories are HSets. -}

id-trans-is-right-neutral : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                          → ⦃ F G : Functor C D ⦄ ⦃ η : NatTrans F G ⦄
                          → proj₁ (η ∙ id-trans F) ≡ proj₁ η
id-trans-is-right-neutral ⦃ F = F ⦄ ⦃ η = (η , _) ⦄ = funext λ x →
  begin
    η x ∘ Id (F # x)
  ≡⟨ IdIsRightNeutral ⟩
    η x
  ∎

id-is-right-neutral : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                    → ⦃ F G : Functor C D ⦄ ⦃ η : NatTrans F G ⦄
                    → η ∙ id-trans F ≡ η
id-is-right-neutral ⦃ F = F ⦄ ⦃ G = G ⦄
  = Σ-h1-≡ (λ η → Π-level-impl
                    (λ x → Π-level-impl
                             (λ y → Π-level-impl
                                      (λ _ → proj₂ (Hom (F # x) (G # y)) _ _))))
           id-trans-is-right-neutral

id-trans-is-left-neutral : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                         → ⦃ F G : Functor C D ⦄ ⦃ η : NatTrans F G ⦄
                         → proj₁ (id-trans G ∙ η) ≡ proj₁ η
id-trans-is-left-neutral ⦃ G = G ⦄ ⦃ η = (η , _) ⦄ = funext λ x →
  begin
    Id (G # x) ∘ η x
  ≡⟨ IdIsLeftNeutral ⟩
    η x
  ∎

id-is-left-neutral : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                   → ⦃ F G : Functor C D ⦄ ⦃ η : NatTrans F G ⦄
                   → id-trans G ∙ η ≡ η
id-is-left-neutral ⦃ F = F ⦄ ⦃ G = G ⦄
  = Σ-h1-≡ (λ η → Π-level-impl
                    (λ x → Π-level-impl
                             (λ y → Π-level-impl
                                      (λ _ → proj₂ (Hom (F # x) (G # y)) _ _))))
           id-trans-is-left-neutral

∙-trans-is-assoc : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
                 → ⦃ F G H K : Functor C D ⦄ ⦃ μ : NatTrans F G ⦄
                 → ⦃ η : NatTrans G H ⦄ ⦃ γ : NatTrans H K ⦄
                 → proj₁ (γ ∙ (η ∙ μ)) ≡ proj₁ ((γ ∙ η) ∙ μ)
∙-trans-is-assoc ⦃ μ = (μ , _) ⦄ ⦃ η = (η , _) ⦄ ⦃ γ = (γ , _) ⦄ = funext λ x →
  begin
    γ x ∘ (η x ∘ μ x)
  ≡⟨ CompIsAssociative ⟩
    (γ x ∘ η x) ∘ μ x
  ∎

∙-is-assoc : ⦃ C : Category {i} {j} ⦄ ⦃ D : Category {k} {l} ⦄
           → ⦃ F G H K : Functor C D ⦄ ⦃ μ : NatTrans F G ⦄
           → ⦃ η : NatTrans G H ⦄ ⦃ γ : NatTrans H K ⦄
           → γ ∙ (η ∙ μ) ≡ (γ ∙ η) ∙ μ
∙-is-assoc ⦃ F = F ⦄ ⦃ K = K ⦄
  = Σ-h1-≡ (λ η → Π-level-impl
                    (λ x → Π-level-impl
                             (λ y → Π-level-impl
                                      λ _ → proj₂ (Hom (F # x) (K # y)) _ _)))
           ∙-trans-is-assoc

[_,_] : (C : Category {i} {j}) (D : Category {k} {l})
      → Category
[ C , D ] = record {
  Ob = Functor C D ;
  Hom = λ F G → (NatTrans ⦃ C ⦄ ⦃ D ⦄ F G) , NatTransIsSet ⦃ C ⦄ ⦃ D ⦄ F G ;
  Id = id-trans ⦃ C ⦄ ⦃ D ⦄ ;
  Comp = λ {F G H} → _∙_ ⦃ C ⦄ ⦃ D ⦄ ⦃ F ⦄ ⦃ G ⦄ ⦃ H ⦄ ;
  IdIsRightNeutral = λ {F G η} → id-is-right-neutral ⦃ C ⦄ ⦃ D ⦄ ⦃ F ⦄ ⦃ G ⦄ ⦃ η ⦄ ;
  IdIsLeftNeutral = λ {F G η} → id-is-left-neutral ⦃ C ⦄ ⦃ D ⦄ ⦃ F ⦄ ⦃ G ⦄ ⦃ η ⦄ ;
  CompIsAssociative = λ {F G H K μ η γ}
    → ∙-is-assoc ⦃ C ⦄ ⦃ D ⦄ ⦃ F ⦄ ⦃ G ⦄ ⦃ H ⦄ ⦃ K ⦄ ⦃ μ ⦄ ⦃ η ⦄ ⦃ γ ⦄ 
  }
