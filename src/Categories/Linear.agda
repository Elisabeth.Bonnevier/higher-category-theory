{-# OPTIONS --without-K #-}

{- This file contains the construction of
the categories corresponding to the totally
ordered finite sets -}

module Categories.Linear where

open import Categories
open import Categories.CAT
open import Categories.Simplex
open import Functors
open import Util.Fin

open import equality using (refl)
open import hott.level using (h↑; h1⇒prop; fin-set)
open import sets.fin using (Fin; zero; suc; toℕ)
open import sets.nat using (ℕ; zero; suc)
open import sets.nat.ordering using (≤-level; refl≤; trans≤)
open import sum using (_,_)

-- 𝕃 n is the category 0 ⟶ 1 ⟶ ... ⟶ n

𝕃 : ℕ → Category
𝕃 n = record {
  Ob = Fin (suc n) ;
  Hom = λ j k → (j ≤ k) , h↑ ≤-level ;
  Id = λ _ → refl≤ ;
  Comp = λ k≤l j≤k → trans≤ j≤k k≤l ;
  IdIsRightNeutral = h1⇒prop ≤-level _ _ ;
  IdIsLeftNeutral = h1⇒prop ≤-level _ _ ;
  CompIsAssociative = h1⇒prop ≤-level _ _
  }

-- 𝕃 defines a functor from Δ to ℂ𝔸𝕋

-- The morphism map
↗-to-functor : {m n : ℕ} → m ↗ n → Functor (𝕃 m) (𝕃 n)
↗-to-functor (f , mono-incr-f) = record {
  ObjectMap = f ;
  MorphismMap = mono-incr-f ;
  RespectsIdentities = h1⇒prop ≤-level _ _ ;
  RespectsComposition = h1⇒prop ≤-level _ _
  }


-- The functor
𝕃[] : Functor Δ ℂ𝔸𝕋
𝕃[] = record {
  ObjectMap = λ n → (𝕃 n) , fin-set _ ;
  MorphismMap = ↗-to-functor ;
  RespectsIdentities = functor≡ refl refl ;
  RespectsComposition = functor≡ refl refl
  }
