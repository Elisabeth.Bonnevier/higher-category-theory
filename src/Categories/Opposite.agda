{-# OPTIONS --without-K #-}

{- This file contains the construction
of the opposite category -}

module Categories.Opposite where

open import Categories

open import equality using (sym)
open import level using (Level)

private
  variable
    i j : Level

opp : (C : Category {i} {j}) → Category {i} {j}
opp C = record {
  Ob = Ob' C ;
  Hom = λ x y → Hom ⦃ C ⦄ y x ;
  Id = λ x → Id ⦃ C ⦄ x ;
  Comp = λ g f → _∘_ ⦃ C ⦄ f g ;
  IdIsRightNeutral = IdIsLeftNeutral ⦃ C ⦄ ;
  IdIsLeftNeutral = IdIsRightNeutral ⦃ C ⦄ ;
  CompIsAssociative = sym (CompIsAssociative ⦃ C ⦄)
  }
