{-# OPTIONS --without-K #-}

{- This file contains the identites
between the standard surjective and
injective maps in the simplex category -}

module Categories.Simplex.Identities where

open import Categories
open import Categories.Simplex
open import Util.Fin
open import Util.Hott
open import Util.Nat

open import decidable
open import equality using (_≡_; refl; sym; _·_; ap; subst; subst₂)
open equality.≡-Reasoning
open import function using (funext) renaming (_∘_ to _∘f_)
open import sets.empty
open import sets.fin
  using (Fin; zero; suc; toℕ; raise; pred; pred-β; _<_; last-fin)
open import sets.nat using (ℕ; zero; suc) renaming (pred to pred-ℕ)
open import sets.nat.ordering as N hiding (_≤_; _<_)

private
  variable
    n : ℕ

-- For n ≥ 2 and 0 ≤ i < j ≤ n we have δ j ∘ δ i = δ i ∘ δ (j - 1)

δjδi=δiδj-1-∀ : (i j : [ suc n ]) → i < j
                → (p : ¬ i ≡ last-fin) → (q : ¬ j ≡ zero)
                → (k : Fin n)
                → δ-fun j (δ-fun (lower i p) k) ≡ δ-fun i (δ-fun (pred j q) k)
δjδi=δiδj-1-∀ i j i<j i≠last j≠0 k
  with toℕ (lower i i≠last) ≤? toℕ k | toℕ (pred j j≠0) ≤? toℕ k
...  | yes i≤k | yes j-1≤k
     with toℕ j ≤? suc (toℕ k) | toℕ i ≤? suc (toℕ k)
...     | yes j≤k+1 | yes i≤k+1 = refl
...     | yes j≤k+1 | no  i≰k+1 = ⊥-elim (i≰k+1 (trans≤ (trans≤ suc≤ i<j) j≤k+1))
...     | no  j≰k+1 | yes i≤k+1 = ⊥-elim (j≰k+1 (subst (λ x → toℕ x N.≤ suc (toℕ k))
                                                       (pred-β j j≠0)
                                                       (s≤s j-1≤k)))
...     | no  j≰k+1 | no  i≰k+1 = refl
δjδi=δiδj-1-∀ i j i<j i≠last j≠0 k
     | yes i≤k | no  j-1≰k
     with toℕ j ≤? suc (toℕ k)
...     | yes j≤k+1 = ⊥-elim (j-1≰k (ap-pred-≤ (subst (λ x → x N.≤ suc (toℕ k))
                                                      (ap toℕ (sym (pred-β j j≠0)))
                                                      j≤k+1)))
...     | no  j≰k+1
        with toℕ i ≤? toℕ (raise k)
...        | yes k≥i = refl
...        | no  i≰k = ⊥-elim (i≰k (subst₂ N._≤_
                                           (sym (toℕ-lower i))
                                           (toℕ-raise k)
                                           i≤k))
δjδi=δiδj-1-∀ i j i<j i≠last j≠0 k
     | no  i≰k | yes j-1≤k = ⊥-elim (i≰k (trans≤ (subst₂ N._≤_ (toℕ-lower i)
                                                               (toℕ-pred j)
                                                               (≤pred i<j))
                                                  j-1≤k))
δjδi=δiδj-1-∀ i j i<j i≠last j≠0 k
     | no  i≰k | no  j-1≰k
     with toℕ j ≤? toℕ (raise k) | toℕ i ≤? toℕ (raise k)
...     | yes j≤k | yes i≤k = refl
...     | yes j≤k | no  k≱i = ⊥-elim (k≱i (trans≤ (<⇒≤ i<j) j≤k))
...     | no  j≰k | yes i≤k = ⊥-elim (i≰k (subst₂ N._≤_
                                                  (toℕ-lower i)
                                                  (sym (toℕ-raise k))
                                                  i≤k))
...     | no  j≰k | no  k≱i = refl

δjδi=δiδj-1 : (i j : [ suc (suc n) ]) → i < j
            → (p : ¬ i ≡ last-fin) → (q : ¬ j ≡ zero)
            → δ j ∘ δ (lower i p) ≡ δ i ∘ δ (pred j q)
δjδi=δiδj-1 i j i<j i≠last j≠0
  = Σ-h1-≡ is-mono-incr-is-h1 (funext (δjδi=δiδj-1-∀ i j i<j i≠last j≠0))


-- For 0 ≤ i ≤ j ≤ n we have σ j ∘ σ i = σ i ∘ σ (j + 1)

σjσi=σiσj+1-∀ : (i j : [ n ]) → i ≤ j
              → (k : [ suc (suc n) ])
              → σ-fun j (σ-fun (raise i) k) ≡ σ-fun i (σ-fun (suc j) k)
σjσi=σiσj+1-∀ i j i≤j k
  with toℕ k ≤? toℕ (raise i) | toℕ k ≤? toℕ (suc j)
...  | yes k≤i | yes k≤j+1
     with toℕ (lower k (≠last k≤i)) ≤? toℕ j | toℕ (lower k (≠last k≤j+1)) ≤? toℕ i
...     | yes k≤j | yes i≥k = ap-fromℕ (sym (toℕ-lower k
                                            · toℕ-lower _)
                                            · toℕ-lower _
                                            · toℕ-lower _)
...     | yes k≤j | no  k≰i = ⊥-elim (k≰i (subst₂ N._≤_
                                                  (toℕ-lower k)
                                                  (sym (toℕ-raise i))
                                                  k≤i))
...     | no  k≰j | yes i≥k = ⊥-elim (k≰j (trans≤ (subst (λ x → toℕ x N.≤ toℕ i)
                                                         (∀lower k)
                                                         i≥k)
                                                  i≤j))
...     | no  k≰j | no  k≰i = ap-fromℕ (sym (toℕ-pred (lower k _))
                                       · ap (pred-ℕ ∘f toℕ) (∀lower k)
                                       · toℕ-pred (lower k _))
σjσi=σiσj+1-∀ i j i≤j k
        | yes k≤i | no  k≰j+1 = ⊥-elim (k≰j+1 (trans≤ k≤i
                                                   (trans≤ (subst (λ x → x N.≤ toℕ j)
                                                                  (toℕ-raise i)
                                                                  i≤j)
                                                           suc≤)))
σjσi=σiσj+1-∀ i j i≤j k
     | no  k≰i | yes k≤j+1
     with toℕ (pred k (≰to≠0 k≰i)) ≤? toℕ j
        | toℕ (lower k (≠last k≤j+1)) ≤? toℕ i
...     | yes k-1≤j | yes k≤i = ⊥-elim (k≰i (subst₂ N._≤_ (sym (toℕ-lower k))
                                                           (toℕ-raise i) k≤i))
...     | yes k-1≤j | no  i≱k = ap-fromℕ (sym (toℕ-lower (pred k _))
                                         · sym (toℕ-pred k)
                                         · ap pred-ℕ (toℕ-lower k)
                                         · toℕ-pred (lower k _))
...     | no  k-1≰j | _       = ⊥-elim (k-1≰j (subst (λ x → x N.≤ toℕ j)
                                                     (toℕ-pred k)
                                                     (≤pred k≤j+1)))
σjσi=σiσj+1-∀ i j i≤j k
     | no  k≰i | no  k≰j+1
     with toℕ (pred k (≰to≠0 k≰i)) ≤? toℕ j
        | toℕ (pred k (≰to≠0 k≰j+1)) ≤? toℕ i
...     | yes k-1≤j | yes k-1≤i = ap-fromℕ (sym (toℕ-lower (pred k _))
                                           · sym (toℕ-pred k)
                                           · toℕ-pred k
                                           · toℕ-lower (pred k _))
...     | yes k-1≤j | no  k-1≰i = ⊥-elim (k≰j+1 (subst (λ x → x N.≤ suc (toℕ j))
                                                (suc-pred
                                                  (λ k=0
                                                    → k≰i
                                                      (subst (λ x → x N.≤ toℕ (raise i))
                                                             (sym k=0)
                                                             z≤n)))
                                                (s≤s (subst (λ x → x N.≤ toℕ j)
                                                            (sym (toℕ-pred k))
                                                            k-1≤j))))
...     | no  k-1≰j | yes k-1≤i = ⊥-elim (k-1≰j (subst (λ x → toℕ x N.≤ toℕ j)
                                                       (∀pred k)
                                                       (trans≤ k-1≤i i≤j)))
...     | no  k-1≰j | no  k-1≰i = ap-fromℕ (sym (toℕ-pred (pred k _))
                                           · ap pred-ℕ (sym (toℕ-pred k)
                                           · toℕ-pred k)
                                           · toℕ-pred (pred k _))

σjσi=σiσj+1 : (i j : [ n ]) → i ≤ j
            → σ j ∘ σ (raise i) ≡ σ i ∘ σ (suc j)
σjσi=σiσj+1 i j i≤j
  = Σ-h1-≡ is-mono-incr-is-h1 (funext (σjσi=σiσj+1-∀ i j i≤j))


-- For 0 ≤ i < j ≤ n we have σ j ∘ δ i = δ i ∘ σ (j - 1)

σj∘δi=δi∘σj-1-∀ : (i j : [ suc n ]) → i < j
                → (p : ¬ j ≡ zero)
                → (k : [ suc n ])
                → σ-fun j (δ-fun (raise i) k) ≡ δ-fun i (σ-fun (pred j p) k)
σj∘δi=δi∘σj-1-∀ i j i<j j≠0 k
  with toℕ (raise i) ≤? toℕ k | toℕ k ≤? toℕ (pred j j≠0)
...  | no  i≰k | no  k≰j-1
  = ⊥-elim (suc≰ {toℕ k} (trans≤ (≰to> i≰k)
                          (trans≤ (<⇒≤ (subst₂ N._<_
                                              (toℕ-raise i)
                                              (sym (ap toℕ (pred-β j j≠0)))
                                  i<j))
                                  (≰to> k≰j-1))))
...  | yes i≤k | yes k≤j-1
     with toℕ (suc k) ≤? toℕ j
...     | no  k+1≰j = ⊥-elim (k+1≰j (subst (λ x → suc (toℕ k) N.≤ x)
                                            (ap toℕ (pred-β j j≠0))
                                            (s≤s k≤j-1)))
...     | yes k+1≤j
        with toℕ i ≤? toℕ (lower k (≠last k≤j-1))
...        | no  i≰k = ⊥-elim (i≰k (subst₂ N._≤_ (sym (toℕ-raise i)) (toℕ-lower k) i≤k))
...        | yes k≥i = ap suc (ap-fromℕ (sym (toℕ-lower k) · toℕ-lower k))
σj∘δi=δi∘σj-1-∀ i j i<j j≠0 k
     | yes i≤k | no  k≰j-1
     with toℕ (suc k) ≤? toℕ j
...     | yes k+1≤j = ⊥-elim (k≰j-1 (≤pred (subst (λ x → suc (toℕ k) N.≤ x)
                                                  (ap toℕ (sym (pred-β j j≠0)))
                                                  k+1≤j)))
...     | no  k+1≰j
        with toℕ i ≤? toℕ (pred k (≰to≠0 k≰j-1))
...        | no  i≰k-1 = ⊥-elim (i≰k-1 (trans≤ (≤pred i<j)
                                                (subst (λ x → pred-ℕ (toℕ j) N.≤ x)
                                                       (toℕ-pred k)
                                                       (≤pred (≤pred (≰to> k+1≰j))))))
...        | yes i≤k-1 = sym (pred-β k (≰to≠0 k≰j-1))
σj∘δi=δi∘σj-1-∀ i j i<j j≠0 k
     | no  i≰k | yes k≤j-1
     with toℕ i ≤? toℕ (lower k (≠last k≤j-1))
...     | yes i≤k = ⊥-elim (i≰k (subst₂ N._≤_ (toℕ-raise i) (sym (toℕ-lower k)) i≤k))
...     | no  k≱i
        with toℕ (raise k) ≤? toℕ j
...        | no  k≰j = ⊥-elim (k≰j (trans≤ (subst (λ x → x N.≤ toℕ (pred j j≠0))
                                                  (toℕ-raise k)
                                                  k≤j-1)
                                           (subst (λ x → x N.≤ toℕ j)
                                                  (toℕ-pred j)
                                                  (≤pred suc≤))))
...        | yes k≤j = ap-fromℕ (sym (toℕ-lower (raise k))
                                · sym (toℕ-raise k)
                                · toℕ-lower k
                                · toℕ-raise (lower k _))

σj∘δi=δi∘σj-1 : (i j : [ suc n ]) → i < j
              → (p : ¬ j ≡ zero)
              → σ j ∘ δ (raise i) ≡ δ i ∘ σ (pred j p)
σj∘δi=δi∘σj-1 i j i<j j≠0
  = Σ-h1-≡ is-mono-incr-is-h1 (funext (σj∘δi=δi∘σj-1-∀ i j i<j j≠0))


-- For 0 ≤ i, j ≤ n with i = j or i = j+1 we have σ j ∘ δ i = id

σi∘δi=id-∀ : (i : [ n ])
           → (k : [ n ])
           → σ-fun i (δ-fun (raise i) k) ≡ k
σi∘δi=id-∀ i k
  with toℕ (raise i) ≤? toℕ k
...  | yes i≤k
     with toℕ (suc k) ≤? toℕ i
...     | yes k+1≤i = ⊥-elim (suc≰ (trans≤ k+1≤i
                                           (subst (λ x → x N.≤ toℕ k)
                                                  (sym (toℕ-raise i))
                                                  i≤k)))
...     | no  k+1≰i = refl
σi∘δi=id-∀ i k
     | no  i≰k
     with toℕ (raise k) ≤? toℕ i
...     | yes k≤i = ap-fromℕ (sym (toℕ-raise k · toℕ-lower (raise k)))
...     | no  k≰i = ⊥-elim (suc≰ {toℕ k} (trans< (≰to> i≰k)
                                                 (subst₂ N._≤_
                                                         (ap suc (toℕ-raise i))
                                                         (sym (toℕ-raise k))
                                                         (≰to> k≰i))))

σi∘δi=id : (i : [ n ])
         → σ i ∘ δ (raise i) ≡ Id n
σi∘δi=id i =  Σ-h1-≡ is-mono-incr-is-h1 (funext (σi∘δi=id-∀ i))

σi∘δi+1=id-∀ : (i : [ n ])
             → (k : [ n ])
             → σ-fun i (δ-fun (suc i) k) ≡ k
σi∘δi+1=id-∀ i k
  with toℕ (suc i) ≤? toℕ k
...  | yes i+1≤k
     with toℕ (suc k) ≤? toℕ i
...     | yes k+1≤i = ⊥-elim (suc≰ (trans< i+1≤k k+1≤i))
...     | no  k+1≰i = refl
σi∘δi+1=id-∀ i k
     | no  i+1≰k
     with toℕ (raise k) ≤? toℕ i
...     | yes k≤i = ap-fromℕ (sym (toℕ-raise k · toℕ-lower (raise k)))
...     | no  k≰i = ⊥-elim (suc≰ {toℕ i} (trans≤ (≰to> k≰i)
                                                 (subst (λ x → x N.≤ toℕ i)
                                                        (toℕ-raise k)
                                                        (≤pred (≰to> i+1≰k)))))

σi∘δi+1=id : (i : [ n ])
           → σ i ∘ δ (suc i) ≡ Id n
σi∘δi+1=id i =  Σ-h1-≡ is-mono-incr-is-h1 (funext (σi∘δi+1=id-∀ i))

-- For 0 ≤ i, j ≤ n with j + 1 < i we have σ j ∘ δ i = δ (i - 1) ∘ σ j

σj∘δi=δi-1∘σj-∀ : (i : [ suc (suc n) ])
                → (j : [ n ])
                → suc (suc j) ≤ i
                → (p : ¬ i ≡ zero)
                → (k : [ suc n ])
                → σ-fun (raise j) (δ-fun i k) ≡ δ-fun (pred i p) (σ-fun j k)
σj∘δi=δi-1∘σj-∀ i j j+2≤i i≠0 k
  with toℕ k ≤? toℕ j | toℕ i ≤? toℕ k
...  | yes k≤j | yes i≤k = ⊥-elim (suc≰ (trans≤ (s≤s k≤j)
                                         (trans≤ suc≤
                                         (trans≤ j+2≤i
                                                 i≤k))))
...  | yes k≤j | no  i≰k
     with toℕ (raise k) ≤? toℕ (raise j)
...     | no  k≰j = ⊥-elim (k≰j (subst₂ N._≤_ (toℕ-raise k) (toℕ-raise j) k≤j))
...     | yes j≥k
        with toℕ (pred i i≠0) ≤? toℕ (lower k (≠last k≤j))
...        | yes i-1≤k = ⊥-elim (suc≰ (trans≤ (subst₂ N._≤_
                                                      (ap (suc ∘f toℕ)
                                                          (pred-β i i≠0))
                                                      (ap (suc ∘f suc)
                                                          (sym (toℕ-lower k)))
                                                      (s≤s (s≤s i-1≤k)))
                                       (trans≤ (s≤s (s≤s k≤j))
                                               j+2≤i)))
...        | no  i-1≰k = ap-fromℕ (sym (toℕ-raise k
                                       · toℕ-lower (raise k))
                                  · toℕ-lower k
                                  · toℕ-raise (lower k _))
σj∘δi=δi-1∘σj-∀ i j j+2≤i i≠0 k
     | no  k≰j | yes i≤k
     with toℕ (pred i i≠0) ≤? toℕ (pred k (≰to≠0 k≰j))
...     | no  i-1≰k-1 = ⊥-elim (i-1≰k-1 (subst₂ N._≤_
                                                (toℕ-pred i)
                                                (toℕ-pred k)
                                                (≤pred i≤k)))
...     | yes i-1≤k-1
        with toℕ (suc k) ≤? toℕ (raise j)
...        | yes k+1≤j = ⊥-elim (suc≰ (trans≤ k+1≤j
                                      (trans≤ suc≤
                                              (subst (λ x → suc x N.≤ toℕ k)
                                                     (toℕ-raise j)
                                                     (≰to> k≰j)))))
...        | no  k+1≰j = sym (pred-β k _)
σj∘δi=δi-1∘σj-∀ i j j+2≤i i≠0 k
     | no  k≰j | no  i≰k
     with toℕ (pred i i≠0) ≤? toℕ (pred k (≰to≠0 k≰j))
...     | yes i-1≤k-1 = ⊥-elim (suc≰ (trans≤ (subst₂ N._≤_
                                                     (ap (suc ∘f toℕ) (pred-β i i≠0))
                                                     (ap (suc ∘f toℕ) (pred-β k _))
                                                     (s≤s (s≤s i-1≤k-1)))
                                             (≰to> i≰k)))
...     | no  i-1≰k-1
        with toℕ (raise k) ≤? toℕ (raise j) -- k ≤? j
...        | yes k≤j = ⊥-elim (suc≰ (trans≤ (≰to> k≰j) (subst₂ N._≤_
                                                               (sym (toℕ-raise k))
                                                               (sym (toℕ-raise j))
                                                               k≤j)))
...        | no  j≱k = ap-fromℕ (sym (ap pred-ℕ (toℕ-raise k)
                                     · toℕ-pred (raise k))
                                · toℕ-pred k
                                · toℕ-raise (pred k _))

σj∘δi=δi-1∘σj : (i : [ suc (suc n) ])
              → (j : [ n ])
              → suc (suc j) ≤ i
              → (p : ¬ i ≡ zero)
              → σ (raise j) ∘ δ i ≡ δ (pred i p) ∘ σ j
σj∘δi=δi-1∘σj i j j+2≤i i≠0
  = Σ-h1-≡ is-mono-incr-is-h1 (funext (σj∘δi=δi-1∘σj-∀ i j j+2≤i i≠0))
