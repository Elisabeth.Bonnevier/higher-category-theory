{-# OPTIONS --without-K #-}

{- Some utilities about finite sets -}

module Util.Fin where

open import equality using (_≡_; refl; ap; subst)
open import sets.empty
open import sets.nat as N hiding (_≤_; pred)
open import sets.fin
  using (Fin; zero; suc; toℕ; raise; last-fin; toℕ-sound; pred)

private
  variable
    m n : ℕ

_≤_ : Fin n → Fin n → Set
i ≤ j = toℕ i N.≤ toℕ j
infixr 4 _≤_

toℕ-raise : ∀ j → toℕ j ≡ toℕ (raise {n} j)
toℕ-raise zero = refl
toℕ-raise (suc j) = ap N.suc (toℕ-raise j)

lower : (i : Fin (suc (suc n)))
      → ¬ (i ≡ last-fin) → Fin (suc n)
lower {n} zero _ = zero
lower {zero} (suc zero) 1≠1 = ⊥-elim (1≠1 refl)
lower {suc n} (suc i) i+1≠last = suc (lower i (λ p → i+1≠last (ap suc p)))

toℕ-lower : (i : Fin (suc (suc n)))
          → {p : ¬ i ≡ last-fin}
          → toℕ i ≡ toℕ (lower {n} i p)
toℕ-lower {n} zero = refl
toℕ-lower {zero} (suc zero) {1≠1} = ⊥-elim (1≠1 refl)
toℕ-lower {suc n} (suc i) {i+1≠last} = ap suc (toℕ-lower i)

∀lower : (i : Fin (suc (suc n)))
       → {p q : ¬ (i ≡ last-fin)}
       → lower i p ≡ lower i q
∀lower {n} zero {p} {q} = refl
∀lower {zero} (suc zero) {p} {q} = ⊥-elim (p refl)
∀lower {suc n} (suc i) {p} {q} = ap suc (∀lower i)

toℕ-pred : (i : Fin (suc n))
         → {p : ¬ i ≡ zero}
         → N.pred (toℕ i) ≡ toℕ (pred i p)
toℕ-pred zero {0≠0} = ⊥-elim (0≠0 refl)
toℕ-pred (suc i) = refl

∀pred : (i : Fin (suc n))
      → {p q : ¬ i ≡ zero}
      → pred i p ≡ pred i q
∀pred zero {p = p} = ⊥-elim (p refl)
∀pred (suc i) = refl

toℕ-last-fin : toℕ (last-fin {n}) ≡ n
toℕ-last-fin {zero} = refl
toℕ-last-fin {suc n} = ap suc toℕ-last-fin

ap-fromℕ : {i j : Fin n}
         → toℕ i ≡ toℕ j
         → i ≡ j
ap-fromℕ {i = zero} {zero} toℕi=toℕj = refl
ap-fromℕ {i = suc i} {suc j} toℕi=toℕj = ap suc (ap-fromℕ (suc-inj toℕi=toℕj))

≠last : {i : Fin (suc n)} {j : Fin (suc (suc n))}
      → toℕ j N.≤ toℕ i → ¬ j ≡ last-fin
≠last {i = i} j≤i refl = suc≰ (trans≤ (s≤s (subst (λ x → x N.≤ toℕ i)
                                                  toℕ-last-fin
                                                  j≤i))
                                      (toℕ-sound i))

≰to≠0 : {i : Fin (suc m)} {j : Fin (suc n)}
      → ¬ (toℕ j N.≤ toℕ i) → ¬ j ≡ Fin.zero
≰to≠0 {i = i} {j = j} j≰i refl = j≰i z≤n

