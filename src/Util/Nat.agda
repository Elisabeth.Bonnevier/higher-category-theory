{-# OPTIONS --without-K #-}

{- Some utilities about natural numbers -}

module Util.Nat where

open import equality using (_≡_; refl)
open import sets.empty
open import sets.nat

private
  variable
    m n : ℕ

≤pred : m ≤ n → pred m ≤ pred n
≤pred z≤n = z≤n
≤pred (s≤s m≤n) = m≤n

≰to> : ¬ m ≤ n → n < m
≰to> {zero} {n} m≰n = ⊥-elim (m≰n z≤n)
≰to> {suc m} {zero} m≰n = z<n
≰to> {suc m} {suc n} m≰n = ap<-suc (≰to> (λ m≤n → m≰n (s≤s m≤n)))

<pred : m < n → m ≤ pred n
<pred {n = suc n} m<n = ap-pred-≤ m<n

suc-pred : ¬ n ≡ zero → suc (pred n) ≡ n
suc-pred {zero} 0≠0 = ⊥-elim (0≠0 refl)
suc-pred {suc n} n+1≠0 = refl
