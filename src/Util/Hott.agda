{-# OPTIONS --without-K #-}

{- Some utilities about h-levels -}

module Util.Hott where

open import equality using (_≡_; unapΣ)
open import hott.level using (h; h1⇒prop)
open import level using (Level)
open import sum using (Σ; _,_; proj₁; proj₂)

private
  variable
    i j : Level

Σ-h1-≡ : {A : Set i} {B : A → Set j}
         → ((x : A) → h 1 (B x))
         → {u v : Σ A B} → proj₁ u ≡ proj₁ v → u ≡ v
Σ-h1-≡ B-is-h1 p = unapΣ (p , h1⇒prop (B-is-h1 _) _ _)


