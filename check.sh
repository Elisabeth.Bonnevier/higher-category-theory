#!/bin/sh
#
# Runs Everything.agda to make sure that
# everything in the repository typechecks.

agda -i src src/Everything.agda
